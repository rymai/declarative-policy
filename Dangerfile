# frozen_string_literal: true

require 'gitlab-dangerfiles'

# 'declarative-policy'
# TODO: roulette uses the project name to find reviewers, but the gitlab team
# directory currently does not have any team members assigned to the declarative-policy
# project. We thus are piggybacking on 'gitlab' for now.
Gitlab::Dangerfiles.for_project(self, 'gitlab') do |gitlab_dangerfiles|
  gitlab_dangerfiles.import_plugins
  gitlab_dangerfiles.config.files_to_category = {
    %r{\A(\.gitlab-ci\.yml\z|\.gitlab/ci)} => :tooling,
    /\Alefthook.yml\z/ => :tooling,
    /\A\.editorconfig\z/ => :tooling,
    /Dangerfile\z/ => :tooling,
    %r{\A(danger/|tooling/danger/)} => :tooling,
    %r{\A?scripts/} => :tooling,
    %r{\Atooling/} => :tooling,
    /(CODEOWNERS)/ => :tooling,
    /\A(Gemfile|Gemfile.lock|Rakefile)\z/ => :backend,
    /\A\.rubocop((_manual)?_todo)?\.yml\z/ => :backend,
    /\.rb\z/ => :backend,
    /(
      \.(md|txt)\z |
      \.markdownlint\.json
    )/x => :docs
  }.freeze

  gitlab_dangerfiles.import_dangerfiles
end
